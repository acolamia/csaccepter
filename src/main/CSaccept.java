package main;

import main.pixels.PixelByColor;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import static main.config.ConfigFile.DELAY_BEFORE_START;
import static main.constants.IntegerConstants.*;
import static main.constants.PathConstants.SCREEN_SAVE_PATH;

public class CSaccept {
    static PixelByColor pixelByColor = new PixelByColor();
    static int cicrleCenterWidth = 0;
    static int cicrleCenterHeight = 0;

    static boolean castedRod = false;
    static boolean definedCircle = false;
    static boolean tryCatched = false;
    static boolean catched = false;
    static int mousePos = 0;

    public static void main(String[] args) throws InterruptedException, AWTException, IOException {
        // Проверяем что путь до файла конфигурации ОК
        checkArgs(args);
        System.out.println("PROGRAM IN PROCESS");

        // Старт приложения
        TimeUnit.SECONDS.sleep(DELAY_BEFORE_START);
        Robot rb = new Robot();
        rb.mouseMove(mousePos, 1440);

        TimeUnit.SECONDS.sleep(DELAY_BEFORE_START);

        do {
            if (!isHOLD_Cast()) {
                rb.keyPress(KeyEvent.VK_F3);
                rb.keyRelease(KeyEvent.VK_F3);
                TimeUnit.SECONDS.sleep(2);
            }
            // Чиним удочку
            repairRod();
            // Закидываем удочку
            if (!catched) {
                castFishingRod();
            }
            // Соглашаемся на вылавливание
            if (!catched && castedRod && definedCircle) {
                acceptToCatchFish();
            }
            // Вылавливаем
            if (!catched && castedRod && definedCircle && tryCatched) {
                catchFish();
            }

            if (castedRod && definedCircle && tryCatched && catched) {
                returnScreen();
            }
            retryCatch();
        } while (true);
    }

    private static void repairRod() throws AWTException, InterruptedException {
        BufferedImage bi = grabScreen();
        if ((pixelByColor.isPureWhite(bi.getRaster().getPixel(3234, 1378, new int[4]))
                && pixelByColor.isPureWhite(bi.getRaster().getPixel(3235, 1378, new int[4]))
                && pixelByColor.isPureWhite(bi.getRaster().getPixel(3234, 1379, new int[4]))
                && pixelByColor.isPureWhite(bi.getRaster().getPixel(3235, 1379, new int[4])))
                &&
                (!pixelByColor.isPureWhite(bi.getRaster().getPixel(3240, 1378, new int[4]))
                        && !pixelByColor.isPureWhite(bi.getRaster().getPixel(3241, 1378, new int[4]))
                        && !pixelByColor.isPureWhite(bi.getRaster().getPixel(3240, 1379, new int[4]))
                        && !pixelByColor.isPureWhite(bi.getRaster().getPixel(3241, 1379, new int[4])))
        ) {
            Robot rb = new Robot();
            rb.setAutoDelay(getRandomDoubleBetweenRange(95, 120));
            rb.keyPress(KeyEvent.VK_TAB);
            rb.keyRelease(KeyEvent.VK_TAB);
            TimeUnit.SECONDS.sleep(1);
            // Навести курсор
            rb.mouseMove(1245, 1370);
            TimeUnit.NANOSECONDS.sleep(500);
            // Нажать ЛКМ (восстановление)
            rb.mousePress(KeyEvent.BUTTON1_MASK);
            rb.mouseRelease(KeyEvent.BUTTON1_MASK);
            TimeUnit.SECONDS.sleep(1);
            // Навести курсор
            rb.mouseMove(1900, 888);
            TimeUnit.NANOSECONDS.sleep(500);
            // Нажать ЛКМ (ДА)
            rb.mousePress(KeyEvent.BUTTON1_MASK);
            rb.mouseRelease(KeyEvent.BUTTON1_MASK);
            TimeUnit.SECONDS.sleep(1);
            // Нажать ТАБ
            rb.keyPress(KeyEvent.VK_TAB);
            rb.keyRelease(KeyEvent.VK_TAB);
            TimeUnit.SECONDS.sleep(1);
            // Нажать Ф3
            rb.keyPress(KeyEvent.VK_F3);
            rb.keyRelease(KeyEvent.VK_F3);
            TimeUnit.SECONDS.sleep(2);
        }
    }

    private static void castFishingRod() throws InterruptedException, AWTException {
        System.out.println("START castFishingRod");
        Robot rb = new Robot();

        if (isHOLD_Cast()) {
            rb.setAutoDelay(getRandomDoubleBetweenRange(2000, 2050));
            rb.keyPress(KeyEvent.VK_F8);
            rb.keyRelease(KeyEvent.VK_F8);
        }
        TimeUnit.SECONDS.sleep(1);
        if (defineFishingCenter()) {
            castedRod = true;
            definedCircle = true;
        }
        System.out.println("FINISH castFishingRod");
    }

    private static boolean defineFishingCenter() {
        BufferedImage bi = grabScreen();
        for (int i = 0; i < 1440; i++) {
            int count = 1000;
            int blackCount = 0;
            for (int j = 1600; j < 1800; j++) {
                if (pixelByColor.isPureWhite(bi.getRaster().getPixel(j, i, new int[4]))) {
                    count = count + 1000;
                }
                if (!pixelByColor.isPureWhite(bi.getRaster().getPixel(j, i, new int[4]))) {
                    if (count > 1000)
                        count--;
                }

                if (count == 21957) {
                    cicrleCenterWidth = j - WIDTH_WHEN_FIND_CIRCLE;
                    cicrleCenterHeight = i + HEIGHT_WHEN_FIND_CIRCLE;

                    BufferedImage bi2 = grabScreen().getSubimage(cicrleCenterWidth - INDENT_TO_TAKE_SQUARE, cicrleCenterHeight - INDENT_TO_TAKE_SQUARE, INDENT_TO_TAKE_SQUARE * 2, INDENT_TO_TAKE_SQUARE * 2);
                    for (int k = 0; k < INDENT_TO_TAKE_SQUARE * 2; k++) {
                        for (int l = 0; l < INDENT_TO_TAKE_SQUARE * 2; l++) {
                            if (pixelByColor.isBlack(bi2.getRaster().getPixel(l, k, new int[4]))) {
                                blackCount++;
                            }
                        }
                    }
                    if (blackCount > 2000) {
                        return true;
                    } else {
                        count = 1000;
                        blackCount = 0;
                    }
                }
            }
        }
        return false;
    }

    private static boolean defineFishingCenterWithoutCoordinates() {
        BufferedImage bi = grabScreen();
        for (int i = 0; i < 1440; i++) {
            int count = 1000;
            for (int j = 1600; j < 1800; j++) {
                if (pixelByColor.isPureWhite(bi.getRaster().getPixel(j, i, new int[4]))) {
                    count = count + 1000;
                }
                if (!pixelByColor.isPureWhite(bi.getRaster().getPixel(j, i, new int[4]))) {
                    if (count > 1000)
                        count--;
                }

                if (count == 21957) {
                    return true;
                }
            }
        }
        return false;
    }

    private static void acceptToCatchFish() throws AWTException {
        System.out.println("START acceptToCatchFish");
        boolean canAccept = true;

        long startTime = new Date().getTime();
        do {
            if (isHOLD_Cast() || (new Date().getTime() - startTime) / 1000 > 40) {
                canAccept = false;
                tryCatched = true;
                break;
            }

            BufferedImage bi = grabScreen().getSubimage(cicrleCenterWidth - INDENT_TO_TAKE_SQUARE, cicrleCenterHeight - INDENT_TO_TAKE_SQUARE, INDENT_TO_TAKE_SQUARE * 2, INDENT_TO_TAKE_SQUARE * 2);
            for (int i = 0; i < INDENT_TO_TAKE_SQUARE * 2; i++) {
                for (int j = 0; j < INDENT_TO_TAKE_SQUARE * 2; j++) {
                    if (pixelByColor.isGreenYellowOrRed(bi.getRaster().getPixel(j, i, new int[4]))) {
                        Robot rb = new Robot();
                        rb.setAutoDelay(getRandomDoubleBetweenRange(90, 120));
                        rb.keyPress(KeyEvent.VK_F8);
                        rb.keyRelease(KeyEvent.VK_F8);
                        tryCatched = true;
                        break;
                    }
                }
                if (tryCatched) break;
            }

            if (!defineFishingCenterWithoutCoordinates() || tryCatched) {
                canAccept = false;
            }
        } while (canAccept || !tryCatched);
        System.out.println("FINISH acceptToCatchFish");
    }

    private static void catchFish() throws InterruptedException, AWTException {
        System.out.println("START catchFish");
        Robot rb = new Robot();
        TimeUnit.SECONDS.sleep(1);
        long startTime = new Date().getTime();

        rb.setAutoDelay(100);
        do {
            if ((new Date().getTime() - startTime) / 1000 > 40) {
                Robot rbForMouse = new Robot();
                rbForMouse.setAutoDelay(55);
                rbForMouse.mousePress(KeyEvent.BUTTON3_MASK);
                rbForMouse.mouseRelease(KeyEvent.BUTTON3_MASK);
                break;
            }

            boolean hasGreen = false;
            if (!isHOLD_Cast()) {
                BufferedImage findGreen = grabScreen().getSubimage(1200, 0, 1000, 1440);
                for (int i = 0; i < 1440; i++) {
                    for (int j = 0; j < 1000; j++) {
                        if (pixelByColor.isYellowOrRed(findGreen.getRaster().getPixel(j, i, new int[4]))) {
                            rb.keyRelease(KeyEvent.VK_F8);
                            hasGreen = true;
                            break;
                        }

                        if (pixelByColor.isGreen(findGreen.getRaster().getPixel(j, i, new int[4]))) {
                            rb.keyPress(KeyEvent.VK_F8);
                            hasGreen = true;
                            break;
                        }
                    }
                    if (hasGreen) {
                        break;
                    }
                }
            } else {
                catched = true;
            }
        } while (!catched);
        System.out.println("FINISH catchFish");
    }

    private static void returnScreen() throws AWTException {
        System.out.println("START returnScreen");
        Robot rb = new Robot();
        int cursorHeight = 0;
        int cursorWidth = 0;
        int greenCount = 0;
        do {
            BufferedImage bi = grabScreen().getSubimage(1695, 25, 50, 40);
            for (int i = 0; i < 40; i++) {
                for (int j = 0; j < 50; j++) {
                    if (pixelByColor.isScreenGreen(bi.getRaster().getPixel(j, i, new int[4]))) {
                        greenCount++;
                    }
                }
            }

            if (greenCount < 10) {
                cursorHeight = cursorHeight + 5;
                rb.mouseMove(cursorHeight, cursorWidth);
                greenCount = 0;
            }
        } while (greenCount < 10);
        rb.mouseMove(cursorHeight, 1440);
        System.out.println("FINISH returnScreen");
    }

    private static BufferedImage grabScreen() {
        try {
            return new Robot().createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
        } catch (SecurityException e) {
        } catch (AWTException e) {
        }
        return null;
    }

    private static void retryCatch() {
        castedRod = false;
        definedCircle = false;
        tryCatched = false;
        catched = false;
    }

    private static void saveScreen(BufferedImage image, String name) {
        try {
            ImageIO.write(image, "png", new File(SCREEN_SAVE_PATH + name + ".png"));
        } catch (IOException e) {
            System.out.println("IO exception" + e);
        }
    }

    public static int getRandomDoubleBetweenRange(double min, double max) {
        double x = (Math.random() * ((max - min) + 1)) + min;
        return (int) x;
    }

    private static boolean isHOLD_Cast() {
        BufferedImage bi = grabScreen();
        if (pixelByColor.isPureWhite(bi.getRaster().getPixel(1889, 959, new int[4]))
                && pixelByColor.isPureWhite(bi.getRaster().getPixel(1899, 959, new int[4]))
                && pixelByColor.isPureWhite(bi.getRaster().getPixel(1905, 959, new int[4]))
                && pixelByColor.isPureWhite(bi.getRaster().getPixel(1912, 959, new int[4]))
                && pixelByColor.isPureWhite(bi.getRaster().getPixel(1918, 959, new int[4]))) {
            return true;
        }
        return false;
    }

    private static void checkArgs(String[] args) throws IOException {
        if (args.length > 0) {
            Path path = Paths.get(args[0]);
            Scanner sc = new Scanner(path);
            sc.useDelimiter(System.getProperty("line.separator"));
            while (sc.hasNext()) {
                String start_after_sec = sc.next();
                if (start_after_sec.contains("START AFTER(sec)")) {
                    DELAY_BEFORE_START = Integer.parseInt(start_after_sec.substring(17));
                }
            }
        }
    }
}
