package main.constants;

public class Color {
    public static final int PURE_WHITE_240 = 240;
    public static final int PURE_WHITE_255 = 255;

    public static final int BLACK_MIN_0 = 21;
    public static final int BLACK_MAX_0 = 64;
    public static final int BLACK_MIN_1 = 21;
    public static final int BLACK_MAX_1 = 64;
    public static final int BLACK_MIN_2 = 21;
    public static final int BLACK_MAX_2 = 64;

    public static final int YELLOW_MIN_0 = 240;
    public static final int YELLOW_MAX_0 = 255;
    public static final int YELLOW_MIN_1 = 200;
    public static final int YELLOW_MAX_1 = 225;
    public static final int YELLOW_MIN_2 = 75;
    public static final int YELLOW_MAX_2 = 90;

    public static final int RED_MIN_0 = 200;
    public static final int RED_MAX_0 = 255;
    public static final int RED_MIN_1 = 55;
    public static final int RED_MAX_1 = 67;
    public static final int RED_MIN_2 = 55;
    public static final int RED_MAX_2 = 67;

    public static final int BROWN_MIN_0 = 230;
    public static final int BROWN_MAX_0 = 255;
    public static final int BROWN_MIN_1 = 125;
    public static final int BROWN_MAX_1 = 145;
    public static final int BROWN_MIN_2 = 40;
    public static final int BROWN_MAX_2 = 60;

    public static final int GREEN_MIN_0 = 50;
    public static final int GREEN_MAX_0 = 80;
    public static final int GREEN_MIN_1 = 180;
    public static final int GREEN_MAX_1 = 255;
    public static final int GREEN_MIN_2 = 130;
    public static final int GREEN_MAX_2 = 205;

    public static final int CATCH_GREEN_MIN_0 = 20;
    public static final int CATCH_GREEN_MAX_0 = 22;
    public static final int CATCH_GREEN_MIN_1 = 180;
    public static final int CATCH_GREEN_MAX_1 = 210;
    public static final int CATCH_GREEN_MIN_2 = 130;
    public static final int CATCH_GREEN_MAX_2 = 160;

    public static final int EXP_COLOR_MIN_0 = 228;
    public static final int EXP_COLOR_MAX_0 = 255;
    public static final int EXP_COLOR_MIN_1 = 135;
    public static final int EXP_COLOR_MAX_1 = 145;
    public static final int EXP_COLOR_MIN_2 = 81;
    public static final int EXP_COLOR_MAX_2 = 89;

    public static final int COORD_GREEN_MIN_0=135;
    public static final int COORD_GREEN_MAX_0=150;
    public static final int COORD_GREEN_MIN_1=245;
    public static final int COORD_GREEN_MAX_1=255;
    public static final int COORD_GREEN_MIN_2=180;
    public static final int COORD_GREEN_MAX_2=190;
}