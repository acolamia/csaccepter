package main.constants;

public class IntegerConstants {
    public static final int WIDTH_WHEN_FIND_CIRCLE = 31;
    public static final int HEIGHT_WHEN_FIND_CIRCLE = 1;
    public static final int INDENT_TO_TAKE_SQUARE = 30;
}