package main.pixels;

import static main.constants.Color.*;

public class PixelByColor {
    public static boolean isPureWhite(int[] pixel) {
        if (pixel[0] >= PURE_WHITE_240 && pixel[0] <= PURE_WHITE_255 && pixel[1] >= PURE_WHITE_240 && pixel[1] <= PURE_WHITE_255 && pixel[2] >= PURE_WHITE_240 && pixel[2] <= PURE_WHITE_255) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isBlack(int[] pixel) {
        if (pixel[0] >= BLACK_MIN_0 && pixel[0] <= BLACK_MAX_0 && pixel[1] >= BLACK_MIN_1 && pixel[1] <= BLACK_MAX_1 && pixel[2] >= BLACK_MIN_2 && pixel[2] <= BLACK_MAX_2) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isGreen(int[] pixel) {
        // check GREEN
        if (pixel[0] >= CATCH_GREEN_MIN_0 && pixel[0] <= CATCH_GREEN_MAX_0 && pixel[1] >= CATCH_GREEN_MIN_1 && pixel[1] <= CATCH_GREEN_MAX_1 && pixel[2] >= CATCH_GREEN_MIN_2 && pixel[2] <= CATCH_GREEN_MAX_2) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isGreenYellowOrRed(int[] pixel) {
        // check GREEN -> YELLOW -> BROWN -> RED
        if ((pixel[0] >= GREEN_MIN_0 && pixel[0] <= GREEN_MAX_0 && pixel[1] >= GREEN_MIN_1 && pixel[1] <= GREEN_MAX_1 && pixel[2] >= GREEN_MIN_2 && pixel[2] <= GREEN_MAX_2)
                || (pixel[0] >= YELLOW_MIN_0 && pixel[0] <= YELLOW_MAX_0 && pixel[1] >= YELLOW_MIN_1 && pixel[1] <= YELLOW_MAX_1 && pixel[2] >= YELLOW_MIN_2 && pixel[2] <= YELLOW_MAX_2)
                || (pixel[0] >= BROWN_MIN_0 && pixel[0] <= BROWN_MAX_0 && pixel[1] >= BROWN_MIN_1 && pixel[1] <= BROWN_MAX_1 && pixel[2] >= BROWN_MIN_2 && pixel[2] <= BROWN_MAX_2)
                || (pixel[0] >= RED_MIN_0 && pixel[0] <= RED_MAX_0 && pixel[1] >= RED_MIN_1 && pixel[1] <= RED_MAX_1 && pixel[2] >= RED_MIN_2 && pixel[2] <= RED_MAX_2)) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isYellowOrRed(int[] pixel) {
        // check GREEN -> YELLOW -> BROWN -> RED
        if ((pixel[0] >= YELLOW_MIN_0 && pixel[0] <= YELLOW_MAX_0 && pixel[1] >= YELLOW_MIN_1 && pixel[1] <= YELLOW_MAX_1 && pixel[2] >= YELLOW_MIN_2 && pixel[2] <= YELLOW_MAX_2)
                || (pixel[0] >= BROWN_MIN_0 && pixel[0] <= BROWN_MAX_0 && pixel[1] >= BROWN_MIN_1 && pixel[1] <= BROWN_MAX_1 && pixel[2] >= BROWN_MIN_2 && pixel[2] <= BROWN_MAX_2)
                || (pixel[0] >= RED_MIN_0 && pixel[0] <= RED_MAX_0 && pixel[1] >= RED_MIN_1 && pixel[1] <= RED_MAX_1 && pixel[2] >= RED_MIN_2 && pixel[2] <= RED_MAX_2)) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isScreenGreen(int[] pixel) {
        if (pixel[0] >= COORD_GREEN_MIN_0 && pixel[0] <= COORD_GREEN_MAX_0 && pixel[1] >= COORD_GREEN_MIN_1 && pixel[1] <= COORD_GREEN_MAX_1 && pixel[2] >= COORD_GREEN_MIN_2 && pixel[2] <= COORD_GREEN_MAX_2) {
            return true;
        } else {
            return false;
        }
    }
}
