package main.config;

import java.awt.event.KeyEvent;

public class ConfigFile {
    public static int TOGGLE_FISHING_MODE = KeyEvent.VK_F3;
    public static int CAST_HOOK_REEL = KeyEvent.VK_F8;
    public static int DELAY_BEFORE_START = 3;
}